#!/bin/bash
set -eu
echo $KUBE_CONFIG | base64 -d > kube.config
export KUBECONFIG=kube.config
TAG=$(date +"%y%m%d")-$CI_COMMIT_SHORT_SHA

NAMESPACE=helm-$CI_COMMIT_SHORT_SHA

kubectl create ns $NAMESPACE

helm install  --name=$NAMESPACE \
 --namespace=$NAMESPACE \
 --set-string persons.url="helm-$TAG.$AUTO_DEVOPS_DOMAIN" \
 --set-string persons.image.tag=$TAG \
 ./cicd/helm/application