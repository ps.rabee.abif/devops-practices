#!/bin/bash
set -eu
echo $KUBE_CONFIG | base64 -d > kube.config
export KUBECONFIG=kube.config
NAMESPACE_SUFFIX=$CI_COMMIT_SHORT_SHA

kubectl delete ns k8s-$NAMESPACE_SUFFIX

helm delete --purge helm-$NAMESPACE_SUFFIX
kubectl delete ns helm-$NAMESPACE_SUFFIX