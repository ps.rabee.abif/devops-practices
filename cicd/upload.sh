#!/bin/bash
set -eu
artifact_tag=$(date +"%y%m%d")-$CI_COMMIT_SHORT_SHA
docker build . -f ./cicd/Dockerfile -t $DOCKER_REGISTRY_USERNAME/devops-practices:$artifact_tag
docker login $DOCKER_REGISTRY_URL -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD
docker push $DOCKER_REGISTRY_USERNAME/devops-practices:$artifact_tag