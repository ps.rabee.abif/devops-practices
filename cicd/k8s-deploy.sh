#!/bin/bash
set -eu
echo $KUBE_CONFIG | base64 -d > kube.config
export KUBECONFIG=kube.config

TAG=$(date +"%y%m%d")-$CI_COMMIT_SHORT_SHA
NAMESPACE=k8s-$CI_COMMIT_SHORT_SHA
sed -i 's/{{URL}}/'k8s-$TAG.$AUTO_DEVOPS_DOMAIN'/' ./cicd/kubernetes/persons-deployment.yaml
sed -i 's/{{TAG}}/'$TAG'/' ./cicd/kubernetes/persons-deployment.yaml
sed -i 's/{{STORAGE_PATH}}/'$NAMESPACE-k8s'/' ./cicd/kubernetes/mysql.yaml

kubectl create ns $NAMESPACE
kubectl apply -n $NAMESPACE -f cicd/kubernetes/db-secret.yaml
kubectl apply -n $NAMESPACE -f cicd/kubernetes/mysql.yaml
kubectl apply -n $NAMESPACE -f cicd/kubernetes/persons-deployment.yaml
